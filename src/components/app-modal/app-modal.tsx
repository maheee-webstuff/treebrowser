import { Component, h, Prop } from '@stencil/core';


@Component({
  tag: 'app-modal',
  styleUrl: 'app-modal.css',
  shadow: true
})
export class AppModal {

  @Prop() header: string;

  render() {
    return (
      <div>
        <div class="backdrop">
        </div>
        <div class="container">
          <header>
            <h1>{this.header}</h1>
          </header>

          <main>
            <slot></slot>
          </main>
        </div>
      </div>
    );
  }
}
