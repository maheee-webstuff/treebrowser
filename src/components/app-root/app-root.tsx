import { Component, h } from '@stencil/core';


@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css',
  shadow: true
})
export class AppRoot {

  root:any = {id: '0', title: 'Item 0', hasChildren: true};

  getChildren = (parent: any): any[] => [0,1,2,3,4,5,6].map(no => ({
    id: parent.id + '-' + no,
    title: parent.title + '-' + no,
    hasChildren: no % 3
  }));

  render() {
    return (
      <div>
        <header>
          <h1>Stencil App Starter</h1>
        </header>

        <main>
          
          <app-modal header="Tree Selection">
            <tree-browser root={this.root} getChildren={this.getChildren}>
            </tree-browser>
          </app-modal>

        </main>
      </div>
    );
  }
}
