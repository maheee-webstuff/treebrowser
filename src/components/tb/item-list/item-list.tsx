import { Component, h, Prop, Event, EventEmitter } from '@stencil/core';


@Component({
  tag: 'tree-browser-item-list',
  styleUrl: 'item-list.css',
  shadow: false
})
export class TreeBrowserItemList {

  @Prop() items: any[] = [];
  @Prop() activeItem: any;
  @Prop() selectedItems: any[];
  @Prop() selectedList: boolean;

  @Event({
    bubbles: false,
    composed: false
  }) listItemClick: EventEmitter;

  @Event({
    bubbles: false,
    composed: false
  }) listItemSelectionToggle: EventEmitter;

  render() {
    return (
      <div>
        {
          this.items.map(item =>
            <tree-browser-item
              header={item.title}
              description={item.description}
              isActive={!this.selectedList && item.id === this.activeItem.id}
              isSelected={!!this.selectedItems.find(i => i.id === item.id)}
              hasChildren={!this.selectedList && item.hasChildren}
              alternativeStyle={this.selectedList}
              onItemClick={ () => item.hasChildren && this.listItemClick.emit(item) }
              onItemSelectionToggle={ () => this.listItemSelectionToggle.emit(item) }>
            </tree-browser-item>
          )
        }
      </div>
    );
  }
}
