import { Component, h, Prop, Event, EventEmitter } from '@stencil/core';


@Component({
  tag: 'tree-browser-item',
  styleUrl: 'item.css',
  shadow: false
})
export class TreeBrowserItem {

  @Prop() header: string;
  @Prop() description: string;
  @Prop() hasChildren: boolean = false;
  @Prop() isActive: boolean = false;
  @Prop() isSelected: boolean = false;
  @Prop() alternativeStyle: boolean; false;
  
  @Event({
    bubbles: false,
    composed: false
  }) itemClick: EventEmitter;

  @Event({
    bubbles: false,
    composed: false
  }) itemSelectionToggle: EventEmitter;

  render() {
    return (
      <div class={'item ' + (this.isActive ? 'active ' : '') + (this.hasChildren ? 'hasChildren ' : '') + (this.isSelected ? 'selected ' : '') + (this.alternativeStyle ? 'alternative ' : '')}>
        <div class="checkbox" onClick={ () => this.itemSelectionToggle.emit() }>
          {
            this.isSelected ? <span>X</span> : <span>O</span>
          }
        </div>
        <div class="content" onClick={ () => this.itemClick.emit() }>
          <div>{this.header}</div>
          <div>{this.description}</div>
        </div>
        <div class="childIndicator" onClick={ () => this.itemClick.emit() }>
          {this.hasChildren ? <span>&gt;</span> : null}
        </div>
      </div>
    );
  }
}
