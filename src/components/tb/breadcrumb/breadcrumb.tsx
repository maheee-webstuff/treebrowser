import { Component, h, Prop } from '@stencil/core';


@Component({
  tag: 'tree-browser-breadcrumb',
  styleUrl: 'breadcrumb.css',
  shadow: false
})
export class TreeBrowserBreadcrumb {

  @Prop() items: any[];

  render() {
    return (
      <div class="breadcrumb">
        {
          this.items.map((item, i, a) =>
            <span>
              <span>{item.title}</span>
              { (a.length - 1 > i) ? <span>&nbsp;&gt;&nbsp;</span> : null }
            </span>
          )
        }
      </div>
    );
  }
}
