import { Component, h, Prop, State, Watch } from '@stencil/core';


@Component({
  tag: 'tree-browser',
  styleUrl: 'tree-browser.css',
  shadow: true
})
export class TreeBrowser {

  @Prop() root: any;
  @Prop() getChildren: (parent: any) => any[];

  @State() stack: any[][];
  @State() next: any[];
  @State() activeItemStack: any = [{}];
  @State() selectedItems: any[] = [];

  @State() columns = {
    start: 0,
    current: 1,
    next: 2,
    movingLeft: false
  };

  @Watch('root')
  rootChanged(newValue: boolean) {
    this.stack = [[newValue]];
    this.next = [];
  }

  componentWillLoad() {
    this.stack = this.root ? [[this.root]] : [[]];
    this.next = [];
  }

  private move(dir: number) {
    let c = this.columns.start + dir;
    while (c < 0) c = c + 4;
    while (c > 3) c = c - 4;
    this.columns = {
      ...this.columns,
      start: c,
      current: (c - 1) % 4,
      next: (c - 2) % 4,
      movingLeft: dir > 0
    };
  }

  moveLeft() {
    this.move(1);
  }
  moveRight() {
    this.move(-1);
  }

  backClicked() {
    if (this.stack.length > 1) {
      this.moveLeft();
      this.next = this.stack[this.stack.length -1];
      this.stack.pop();
      this.activeItemStack.pop();
      this.activeItemStack = [...this.activeItemStack];
    }
  }

  itemClicked(column: number, item: any) {
    if (column === 1) {
      this.next = this.getChildren(item);
      this.activeItemStack[this.activeItemStack.length - 1] = item;
    } else if (column === 2) {
      this.moveRight();
      this.stack.push(this.next);
      this.next = this.getChildren(item);
      this.activeItemStack.push(item);
    }
    this.activeItemStack = [...this.activeItemStack];
  }

  itemSelectionToggled(column: number, item: any) {
    if (this.selectedItems.find(i => i.id === item.id)) {
      this.selectedItems = this.selectedItems.filter(i => i.id !== item.id);
    } else {
      this.selectedItems = [...this.selectedItems, item];
    }
    this.selectedItems.sort((a, b) => a.title.localeCompare(b.title));
  }

  render() {
    const classes = ['', '', '', ''];
    const stacks = [[], [], [], []];
    const ids = [-1, -1 -1, -1];
    for (let i = 0; i < 4; ++i) {
      const n = (i + this.columns.start) % 4;
      ids[i] = n;
      classes[i] = 'treeItems treeItems-' + n;
      if ((n === 0 && this.columns.movingLeft) || (n === 3 && !this.columns.movingLeft)) {
        classes[i] = classes[i] + ' hide';
      }
      if (n === 0) {
        stacks[i] = this.stack.length > 1 ? this.stack[this.stack.length - 2] : [];
      } else if (n === 1) {
        stacks[i] = this.stack[this.stack.length - 1];
      } else if (n === 2) {
        stacks[i] = this.next || [];
      }
    }

    const activeItem = this.activeItemStack[this.activeItemStack.length - 1];

    return (
      <div class="container">
        <div class="header">
          <tree-browser-breadcrumb items={this.activeItemStack}>
          </tree-browser-breadcrumb>
          <button disabled={this.stack.length <= 1} onClick={ () => this.backClicked() }>BACK</button>
        </div>
        <div class="body">
          <div class="tree">
            <div class={classes[0]}>
              <tree-browser-item-list
                items={stacks[0]}
                activeItem={activeItem}
                selectedItems={this.selectedItems}
                onListItemClick={ (event) => this.itemClicked(ids[0], event.detail) }
                onListItemSelectionToggle={ (event) => this.itemSelectionToggled(ids[0], event.detail) }>
              </tree-browser-item-list>
            </div>
            <div class={classes[1]}>
              <tree-browser-item-list
                items={stacks[1]}
                activeItem={activeItem}
                selectedItems={this.selectedItems}
                onListItemClick={ (event) => this.itemClicked(ids[1], event.detail) }
                onListItemSelectionToggle={ (event) => this.itemSelectionToggled(ids[0], event.detail) }>
              </tree-browser-item-list>
            </div>
            <div class={classes[2]}>
              <tree-browser-item-list
                items={stacks[2]}
                activeItem={activeItem}
                selectedItems={this.selectedItems}
                onListItemClick={ (event) => this.itemClicked(ids[2], event.detail) }
                onListItemSelectionToggle={ (event) => this.itemSelectionToggled(ids[0], event.detail) }>
              </tree-browser-item-list>
            </div>
            <div class={classes[3]}>
              <tree-browser-item-list
                items={stacks[3]}
                activeItem={activeItem}
                selectedItems={this.selectedItems}
                onListItemClick={ (event) => this.itemClicked(ids[3], event.detail) }
                onListItemSelectionToggle={ (event) => this.itemSelectionToggled(ids[0], event.detail) }>
              </tree-browser-item-list>
            </div>
          </div>
          <div class="selection">
            <div>
              <tree-browser-item-list
                  items={this.selectedItems}
                  activeItem={null}
                  selectedItems={this.selectedItems}
                  selectedList={true}
                  onListItemSelectionToggle={ (event) => this.itemSelectionToggled(-1, event.detail) }>
              </tree-browser-item-list>
            </div>
          </div>
        </div>


      </div>
    );
  }
}
