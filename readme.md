# Stencil App Starter

- https://maheee-webstuff.gitlab.io/treebrowser/

## Getting Started

```bash
npm install
```

```bash
npm start
```

To build the app for production, run:

```bash
npm run build
```
